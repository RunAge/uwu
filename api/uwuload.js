const e = require('express');
const router = e.Router();
const multer  = require('multer');
const fs = require('fs');
const path = require('path');
const { promisify } = require('util');

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname)
  }
})

const upload = multer({ storage: storage });
const readDirPromise = promisify(fs.readdir);
const unlinkPromise = promisify(fs.unlink);
const statPromise = promisify(fs.stat);
const renamePromise = promisify(fs.rename);

router.route('/')

  .get(async (req, res) => {
    const files = await readDirPromise('uploads/');
    res.json({
      code: 200,
      files
    })
  })

  .post(upload.array('files'), (req, res) => {
    res.json({
      code: 200,
      files: req.files
    })
  })

  .delete(async (req, res) => {
    const files = await readDirPromise('uploads/');
    for(file of files) {
      await unlinkPromise(path.join(process.cwd(), 'uploads/', file))
    }
    res.json({
      code: 200,
      message: 'Files removed.'
    })
  });

router.route('/:filename')
  .get(async (req, res) => {
    try {
      statPromise(path.join(process.cwd(), 'uploads/', req.params.filename));
      res.sendFile(path.join(process.cwd(), 'uploads/', req.params.filename));
    } catch (error) {
      res.json({
        code: 404,
        message: 'Not Found'
      })
    }
  })
  .put(async (req, res) => {
    try {
      if(!req.body.newName) {
        throw new Error('No parameter newName');
      }
      const file = path.join(process.cwd(), 'uploads/', req.params.filename)
      const newFile = path.join(process.cwd(), 'uploads/', req.body.newName)
      await statPromise(file);
      renamePromise(file, newFile);
      res.json({
        code: 200,
        message: `Name of ${req.params.filename} changed to ${req.body.newName}`
      });
    } catch(err) {
      res.json({
        code: 404,
        message: 'Not Found',
        error: err
      })
    }

  })
  .delete(async (req, res) => {
    try {
      await unlinkPromise(path.join(process.cwd(), 'uploads/', req.params.filename));
      res.json({
        code: 200,
        message: `${req.params.filename} removed.`
      })
    } catch (error) {
      res.json({
        code: 404,
        message: 'Not Found'
      })
    }
  })
module.exports = router;
