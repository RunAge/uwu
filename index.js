const express = require('express');
const http = require('http');
const bodyParser = require('body-parser')
const path = require('path');
const api = require('./api');

const app = express();
const server = http.createServer(app);
server.listen(1337, 'localhost', () => {
  console.log('UwU started on http://localhost:1337');
})

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(express.static('public'));

app.get('/', (req, res) => {
  res.sendFile(path.join(process.cwd(), 'pages/uwu.html'));
});
app.use('/api', api);
